﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsCommand
{
    public partial class Form1 : Form
    {
        TextBox txt;
        ComboBox cmb;

        Invoker invoker = new Invoker();

        public Form1(/*Invoker invoker*/)
        {

            InitializeComponent();

            // Set properties form
            this.Width = 360;
            this.Height = 450;
            this.StartPosition = FormStartPosition.CenterScreen;

            //Command Pattern
            Receiver receiver = new Receiver();
            this.invoker = new Invoker();
            this.invoker.addCommand(new FreelancerCommand(receiver));
            this.invoker.addCommand(new PoliticianCommand(receiver));
            this.invoker.addCommand(new ProgrammerCommand(receiver));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AddComboBox();
            AddTextBox();
            addLabel("COMMAND Set", 20, 15, 300, 20);
        }
        
        public TextBox AddTextBox ()
        {
            txt = new TextBox();
            this.Controls.Add(txt);
            txt.Multiline = true;
            txt.ScrollBars = ScrollBars.Vertical;
            txt.AcceptsReturn = true;
            txt.AcceptsTab = true;
            txt.WordWrap = true;
            txt.SetBounds(20, 60, 300,300);
            txt.Text = "Command Shell\r\n\r\n";
            return txt;
        }

        public Label addLabel(string title, int x, int y, int width, int height)
        {
            Label lbl = new Label();
            lbl.Text = title;
            lbl.SetBounds(x, y, width, height);
            this.Controls.Add(lbl);
            return lbl;
        }

        public ComboBox AddComboBox()
        {
            cmb = new ComboBox();
            cmb.SelectedIndexChanged += new EventHandler(CommandList_SelectedIndexChanged);
            this.Controls.Add(cmb);
            cmb.SetBounds(20, 30, 300, 10);
            String[] array = invoker.keys();
            for(int i = 0; i < array.Length; i++)
            {
                cmb.Items.Add(array[i]);
            }
            return cmb;
        }

        private void CommandList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var commandtext = ((ComboBox)sender).SelectedItem;
            txt.Text = txt.Text + "$ " + invoker.execute(commandtext.ToString()) + "\r\n";
        }

        
    }
}
