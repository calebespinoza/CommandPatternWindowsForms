﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsCommand
{
    class Receiver
    {
        public string JobDescriptionFreelancer()
        {
            return "Take out the trash.";
        }

        public string JobDescriptionPolitician()
        {
            return "Take money from the rich people, take votes from the poor people.";
        }

        public string JobDescriptionProgrammer()
        {
            return "Sell the bugs, charge extra for the fixes";
        }
    }
}
