﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsCommand
{
    class PoliticianCommand : ICommand
    {
        private Receiver receiver;

        public PoliticianCommand(Receiver obj)
        {
            this.receiver = obj;
        }

        public string commandName()
        {
            return "Command: setCommandToPolitician";
        }

        public string Execute()
        {
            return this.receiver.JobDescriptionPolitician();
        }
    }
}
