﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsCommand
{
    class FreelancerCommand : ICommand
    {
        private Receiver receiver;

        public FreelancerCommand(Receiver obj)
        {
            this.receiver = obj;
        }

        public string commandName()
        {
            return "Command: setCommandToFreelancer";
        }

        public string Execute()
        {
            return this.receiver.JobDescriptionFreelancer();
        }
    }
}
