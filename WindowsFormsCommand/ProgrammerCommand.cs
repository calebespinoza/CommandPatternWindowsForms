﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsCommand
{
    class ProgrammerCommand : ICommand
    {
        private Receiver receiver;

        public ProgrammerCommand(Receiver obj)
        {
            this.receiver = obj;
        }

        public string commandName()
        {
            return "Command: setCommandToProgrammer";
        }

        public string Execute()
        {
            return this.receiver.JobDescriptionProgrammer();
        }
    }
}
