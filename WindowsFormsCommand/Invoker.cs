﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsCommand
{
    public class Invoker
    {
        Dictionary<string, ICommand> dictionaryCommands;

        public Invoker()
        {
            dictionaryCommands = new Dictionary<string, ICommand>();
        }

        public void addCommand(ICommand command)
        {
            this.dictionaryCommands.Add(command.commandName(), command);
        }

        public string execute(string commandName)
        {
            return dictionaryCommands[commandName].Execute();
        }

        public string[] keys()
        {
            return this.dictionaryCommands.Keys.ToArray();
        }

    }
}
